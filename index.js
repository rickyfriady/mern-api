const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const multer = require("multer");
const path = require("path");

const app = express();
const authRoutes = require("./src/routes/auth");
const blogRoutes = require("./src/routes/blog");

//menyimpan file upload dari sisi client
const fileStorage = multer.diskStorage({
  destination: (req, file, callB) => {
    callB(null, "images");
  },
  filename: (req, file, callB) => {
    callB(null, new Date().getTime() + "-" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/png" || file.mimetype === "image/jpg" || file.mimetype === "image/jpeg") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

//! Midelware area

//___dirname adalah lokasi dimana proyek kita berada
app.use("/images", express.static(path.join(__dirname, "images")));
//single('image') adalah data body parser yang bernama image
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single("image"));

//menerima data body parser berupa json
app.use(bodyParser.json());

// Mengatasi Error CORS ORIGIN pada API
app.use((req, res, next) => {
  //   req.setHeader("Access-Control-Allow-Origin", "codepen.io");
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

// Groupimg path
app.use("/V1/auth", authRoutes);
app.use("/V1/blog", blogRoutes);

//middleware error
app.use((error, req, res, next) => {
  const status = error.errorStatus || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ message: message, data: data });
});

//connection to mongoose
mongoose
  .connect("mongodb+srv://ricky:7eAVCVGliG4TLKOt@cluster0.z0ctw.mongodb.net/mernBlogDB?retryWrites=true&w=majority")
  .then(() => {
    app.listen(4000, () => {
      console.log("Connection to MonggoDb Success");
    });
  })
  .catch((err) => console.log(err));

//! end middleware area
