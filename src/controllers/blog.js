const { validationResult } = require("express-validator");
const path = require("path");
const fs = require("fs");

const Blogpost = require("../models/blog");
const { count } = require("console");

exports.createBlogPost = (req, res, next) => {
  const errors = validationResult(req);

  //cek error inputan
  if (!errors.isEmpty()) {
    const err = new Error("input value tidak sesuai");
    err.errorStatus = 400;
    err.data = errors.array();
    throw err;
  }
  //cek file upload dari client
  if (!req.file) {
    const err = new Error("Image harus di Upload");
    err.errorStatus = 422;
    throw err;
  }

  const title = req.body.title;
  const image = req.file.path;
  const body = req.body.body;

  const Postingan = new Blogpost({
    title: title,
    body: body,
    image: image,
    author: { uid: 1, name: "ricki friadi" },
  });

  Postingan.save()
    .then((result) => {
      res.status(201).json({
        message: "Create Blog Post Success",
        data: result,
      });
    })
    .catch((err) => {
      console.log("error : ", err);
    });
};

exports.getAllBlogPost = (req, res, next) => {
  const currentPage = req.query.page || 1;
  const perPage = req.query.perPage || 5;
  let totalItems;

  Blogpost.find()
    .countDocuments()
    .then((count) => {
      totalItems = count;
      return Blogpost.find()
        .skip((parseInt(currentPage) - 1) * parseInt(perPage))
        .limit(parseInt(perPage));
    })
    .then((result) => {
      res.status(200).json({
        message: "Data Blog Post Berhasil Di Panggil",
        data: result,
        total_Data: totalItems,
        per_Page: parseInt(perPage),
        current_Page: parseInt(currentPage),
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.getBlogPostById = (req, res, next) => {
  const postId = req.params.postId;
  Blogpost.findById(postId)
    .then((result) => {
      // cek Id ke database
      if (!result) {
        const error = new Error("Blog Post tidak ditemukan");
        error.errorStatus = 404;
        throw error;
      }
      res.status(200).json({
        message: "Data Blog post Berhasil di Panggil",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateBlogPost = (req, res, next) => {
  const errors = validationResult(req);

  //cek error inputan
  if (!errors.isEmpty()) {
    const err = new Error("input value tidak sesuai");
    err.errorStatus = 400;
    err.data = errors.array();
    throw err;
  }
  //cek file upload dari client
  if (!req.file) {
    const err = new Error("Image harus di Upload");
    err.errorStatus = 422;
    throw err;
  }

  const title = req.body.title;
  const image = req.file.path;
  const body = req.body.body;
  const postId = req.params.postId;

  Blogpost.findById(postId)
    .then((post) => {
      // cek postingan dapat atau tidak
      if (!post) {
        const err = new Error("Blog Post Tidak Di Temukan");
        err.errorStatus = 404;
        throw err;
      }
      // ganti value dari mongodb dengan yang baru
      post.title = title;
      post.body = body;
      post.image = image;

      return post.save();
    })
    // multi promise untuk then return post.save() yang diatas
    .then((result) => {
      res.status(200).json({
        message: "Data Blog Berhasil di Update",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteBlogPost = (req, res, next) => {
  const postId = req.params.postId;

  Blogpost.findById(postId)
    .then((post) => {
      if (!post) {
        const err = new Error("Blog Post Tidak Di Temukan");
        err.errorStatus = 404;
        throw err;
      }
      //panggil fungsi removeImage
      removeImage(post.image);
      //menghapus post berserta image di directori
      return Blogpost.findByIdAndRemove(postId);
    })
    .then((result) => {
      res.status(200).json({
        message: "Hapus Blog Post Berhasil!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

const removeImage = (filePath) => {
  console.log("filePatch : ", filePath);
  console.log("dir filename : ", __dirname);

  // menggabungkan file path image dengan dirproject
  // C:\Users\M-S-I\Documents\projectricki\MERN\mern-blog-api\images\{file image}
  filePath = path.join(__dirname, "../..", filePath);
  //remove image dari directori system
  fs.unlink(filePath, (err) => console.log(err));
};
