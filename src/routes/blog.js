const express = require("express");
const { body } = require("express-validator");

const router = express.Router();

//controller
const blogController = require("../controllers/blog");

//[Post] create post: /v1/blog/post
//TODO : MENAMBAHKAN VALIDASI DARI DATA INPUT USER
router.post(
  "/post",
  [body("title").isLength({ min: 5 }).withMessage("input title anda tidak sesuai karena kurang dari 5 huruf"), body("body").isLength({ min: 5 }).withMessage("input body anda tidak sesuai karena kurang dari 5 huruf")],
  blogController.createBlogPost
);

//[Get] all post : /v1/blog/posts
router.get("/posts", blogController.getAllBlogPost);
//[Get] one post : /v1/blog/post/:postId
router.get("/post/:postId", blogController.getBlogPostById);
//[PUT] Update Post : /v1/blog/post/:postId
router.put(
  "/post/:postId",
  [body("title").isLength({ min: 5 }).withMessage("input title anda tidak sesuai karena kurang dari 5 huruf"), body("body").isLength({ min: 5 }).withMessage("input body anda tidak sesuai karena kurang dari 5 huruf")],
  blogController.updateBlogPost
);
//[Delete] Delete Post : /v1/blog/post/:postId
router.delete("/post/:postId", blogController.deleteBlogPost);
module.exports = router;
